/**
 * ---------------------------------------------------------------------
 * $Id: ArbolBinarioBusqueda.java,v 1.0 2013/08/23
 * Universidad Francisco de Paula Santander
 * Programa Ingenieria de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */
package util;

import java.util.Iterator;

/**
 * Implementacion de Clase para el manejo de un Arbol Binario de busqueda
 * (ordenado). <br>
 *
 * @param <T> Tipo de dato a almacenar en el Arbol Binario de Busqueda. <br>
 * @author Marco Adarme
 * @version 2.0
 */
public class ArbolBinarioBusqueda<T> extends ArbolBinario<T> {

    ////////////////////////////////////////////////////////////
    // ArbolBinarioDeBusqueda  - Implementacion de Metodos /////
    ////////////////////////////////////////////////////////////
    /**
     * Crea un Arbol Binario de Busqueda vacio. <br>
     * <b>post: </b> Se creo un Arbol Binario de Busqueda vacio. <br>
     */
    public ArbolBinarioBusqueda() {
        super();
    }

    /**
     * Crea un arbol con una raiz predefinida. <br>
     * <b>post: </b> Se creo un Arbol Binario de Busqueda con raiz
     * predeterminada. <br>
     *
     * @param raiz un tipo T, almacena la direccion de memoria de un nodo de un
     * Arbol Binario de Busqueda. <br>
     */
    public ArbolBinarioBusqueda(T raiz) {
        super(raiz);
    }

    @Override
    public NodoBin<T> getRaiz() {
        return (super.getRaiz());
    }

    //------------------->-------------> START MY IMPLEMENTATION -------->-------------->-------------->------------------>-
    /**
     * Metodo que retorna el papa o nodo raiz de un T info
     *
     * @return el papa de la info ( funciona )
     */
    public T papa(T info) {
        NodoBin<T> r = this.getRaiz();
        System.out.println(r.getInfo());
        return papa(info, r);

    }

    private T papa(T info, NodoBin<T> r) {

        if (r == null) {
            return null;
        }
        if ((r.getIzq() != null && r.getIzq().getInfo().equals(info)) || (r.getDer() != null && r.getDer().getInfo().equals(info))) {
            return r.getInfo();
        }

        // T aux que guarde la informacion que obtuvo por la izquierda y si es nula pues que la actualice a la derecha
        T aux = this.papa(info, r.getIzq());
        if (aux == null) {
            return this.papa(info, r.getDer());
        } else {
            return aux;
        }

    }

    /**
     * 4. Se define el área de un árbol binario A como el número de hojas
     * multiplicado por su altura. Implementar un método para determinar si dos
     * árboles binarios diferentes tiene la misma área.
     */
    public boolean getArea(ArbolBinario<T> otro) {
        NodoBin<T> r = this.getRaiz();
        return getArea(r, otro);

    }

    private boolean getArea(NodoBin<T> r, ArbolBinario<T> otro) {
        if (r == null || otro.getRaiz() == null) {
            return false; // alguno de los arboles es nulo 
        }

        int area1 = this.contarHojas() * this.getAltura();
        int area2 = otro.contarHojas() * otro.getAltura();

        if (area1 == area2) {
            return true;
        }
        return false;
    }

    /**
     * ejercicio 5 : dado un arbol binario y un info que se encuentre almacenado
     * en el , elaborar un metod que determine el nivel del info , si el info no
     * esta return -1
     */
    public int getNivel(T info) {
        int nivel = 0;
        NodoBin<T> r = this.getRaiz();
        return getNivel(r, info, nivel);

    }

    private int getNivel(NodoBin<T> r, T info, int nivel) {
        if (r == null) {
            return -1;
        }
        if (r.getInfo().equals(info)) {
            return nivel;
        }
        // no encontro la info en la raiz 
        int aux = this.getNivel(r.getIzq(), info, nivel + 1);
        if (aux == -1) {
            aux = this.getNivel(r.getDer(), info, nivel + 1);
        }

        return aux;

    }

    /**
     * ejercicio propuesto : dado un nivel , imprima todos los elementos de ese
     * nivel
     *
     */
    public void impNivel(int nivelObj) {

        NodoBin<T> r = this.getRaiz();
        impNivel(r, 0, nivelObj);
    }

    private void impNivel(NodoBin<T> r, int nivel, int nivelObj) {
        if (r == null) {
            return;
        }
        impNivel(r.getIzq(), nivel + 1, nivelObj);

        if (nivel == nivelObj) {
            System.out.println(r.getInfo());
        }

        // ya termino por la izquierda , valla por la derecha 
        impNivel(r.getDer(), nivel + 1, nivelObj);

    }

    /**
     * 6. Dado un árbol binario y un dato n que determina el nivel, retornar una
     * lista con los elementos de los nodos que pertenecen al nivel n. Lanzar
     * una excepción si “n” >= a la altura del árbol o n < 0 funciona
     */
    public ListaS<T> impLista(int nivel) throws Exception {
        if (nivel < 0 || nivel >= this.getAltura()) {
            throw new Exception("error ");
        } else {

            NodoBin<T> r = this.getRaiz();
            ListaS<T> lista = new ListaS();
            return impLista(r, 0, nivel, lista);
        }
    }

    private ListaS<T> impLista(NodoBin<T> r, int nivel, int nivelObj, ListaS<T> lista) {
        if (r == null) {
            return lista;
        }

        lista = impLista(r.getIzq(), nivel + 1, nivelObj, lista);
        if (nivel == nivelObj) {
            lista.insertarAlInicio(r.getInfo());
        }
        // ya termino por la izquierda , valla por la derecha 
        lista = impLista(r.getDer(), nivel + 1, nivelObj, lista);
        return lista;
    }

    /**
     * 7. Dado un árbol binario elaborar un método que calcule la altura del
     * árbol. ( funciona )
     */
    public int getAltura2() {
        NodoBin<T> r = this.getRaiz();
        return getAltura2(r);

    }

    private int getAltura2(NodoBin<T> r) {
        if (r == null) {
            return -1;
        }

        int ai = 0, ad = 0; // no vuelven a inicializarse en 0 ? 
        if (r.getIzq() != null) {
            ai = getAltura2(r.getIzq());
        }

        if (r.getDer() != null) {
            ad = getAltura2(r.getDer());
        }

        if (ai >= ad) {
            return ai + 1;
        }
        return ad + 1;

    }

    /**
     * 8. Dado un árbol binario y un dato “info” que se encuentra almacenado en
     * él, elaborar un método que retorne en una lista el camino entre la raíz y
     * “info”. Lanzar una excepción en el caso que “info” no exista dentro del
     * árbol o sea null. ( funciona )
     */
    public ListaS<T> camino(T info) throws Exception {
        /*
        NodoBin<T> r = this.getRaiz();
        ListaS<T> lista = new ListaS<>();
        return this.camino(r, info, lista);
         */
        NodoBin<T> r = this.getRaiz();
        if (this.esta(r, info)) {

            ListaS<T> lista = new ListaS<>();

            return this.camino(r, info, lista);
        } else {
            throw new Exception("la info no se encuentra en el arbol.");
        }

    }

    private ListaS<T> camino(NodoBin<T> r, T info, ListaS<T> lista) {
        if (r == null) {
            return lista;
        }

        if (!r.getInfo().equals(info)) {
            lista.insertarAlFinal(r.getInfo());
        }
        int c = ((Comparable) (info)).compareTo(r.getInfo());

        if (c < 0) {
            // por la izquierda
            camino(r.getIzq(), info, lista);

        } else if (c > 0) {
            //por la derecha 
            camino(r.getDer(), info, lista);

        } else {
            lista.insertarAlFinal(info);
            return lista;
        }

        return lista;
    }

    public ListaS<T> caminosInfos(T info1, T info2) throws Exception {

        ListaS<T> lista1 = this.camino(info1);
        ListaS<T> lista2 = this.camino(info2);
        ListaS<T> listaT = new ListaS<>();

        T vector1[] = (T[]) lista1.aVector();
        T vector2[] = (T[]) lista2.aVector();

        int i = 0;
        int length = Math.min(vector1.length, vector2.length);
        while (i < length - 1 && vector1[i + 1].equals(vector2[i + 1])) {
            i++;
        }

        for (int j = vector1.length - 1; j >= i; j--) {
            listaT.insertarAlFinal(vector1[j]);
        }

        for (int j = i + 1; j < vector2.length; j++) {
            listaT.insertarAlFinal(vector2[j]);
        }

        return listaT;

    }

    /**
     * 10. Dada un árbol binario, elaborar un método que devuelva en una lista
     * Simple el recorrido en Preorden, esté método se debe implementar de
     * manera iterativa. ( funciona )
     */
    public ListaS<T> preOrden2() {
        NodoBin<T> r = this.getRaiz();
        return this.preOrden2(r);

    }

    private ListaS<T> preOrden2(NodoBin<T> r) {
        ListaS<T> lista = new ListaS<>();
        Pila<NodoBin<T>> pila = new Pila<>();

        pila.apilar(r);

        while (!pila.esVacia()) {
            r = pila.desapilar();
            lista.insertarAlFinal(r.getInfo());

            if (r.getDer() != null) {
                pila.apilar(r.getDer());

            }

            if (r.getIzq() != null) {
                pila.apilar(r.getIzq());
            }

        }
        return lista;
    }

    /**
     * 11. Dada un árbol binario, elaborar un método que devuelva en una lista
     * Simple el recorrido en Inorden, esté método se debe implementar de manera
     * iterativa. ( funciona ! )
     */
    public ListaS<T> inorden2() {
        NodoBin<T> r = this.getRaiz();
        return inorden2(r);

    }

    private ListaS<T> inorden2(NodoBin<T> r) {

        ListaS<T> lista = new ListaS<>();
        Pila<NodoBin<T>> pila = new Pila<>();

        NodoBin<T> actual = r;

        while (actual != null || !pila.esVacia()) {
            while (actual != null) {
                pila.apilar(actual);
                actual = actual.getIzq();
            }

            if (!pila.esVacia()) {
                actual = pila.desapilar();
                lista.insertarAlFinal(actual.getInfo());
                actual = actual.getDer();
            }

        }
        return lista;
    }

    /**
     * 12. Dada un árbol binario, elaborar un método que devuelva en una lista
     * Simple el recorrido en Postorden, esté método se debe implementar de
     * manera iterativa. ( funciona perfecto , la pila simula la recursion )
     */
    public ListaS<T> postOrden2() {
        NodoBin<T> r = this.getRaiz();
        return postOrden2(r);
    }

    private ListaS<T> postOrden2(NodoBin<T> r) {
        // crear estructuras de datos auxiliares 
        ListaS<T> lista = new ListaS<>();
        Pila<NodoBin<T>> pila = new Pila<>();

        if (r == null) {
            return lista; // retorne la lista vacia 
        }
        pila.apilar(r);

        while (!pila.esVacia()) {

            r = pila.desapilar();
            lista.insertarAlInicio(r.getInfo());

            if (r.getIzq() != null) {
                pila.apilar(r.getIzq());

            }

            if (r.getDer() != null) {
                pila.apilar(r.getDer());
            }
        }
        return lista;

    }

    public boolean mismos(ArbolBinarioBusqueda<T> otro) {
        NodoBin<T> r = this.getRaiz();
        NodoBin<T> r2 = otro.getRaiz();

        return mismos(r, r2);

    }

    private boolean mismos(NodoBin<T> r, NodoBin<T> r2) {
        if (r == null) {
            return r2 == null;
        }
        if (r2 == null) {
            return false;
        }

        if (r.getInfo().equals(r2.getInfo())) {
            boolean b1 = this.mismos(r.getIzq(), r2.getIzq());
            boolean b2 = this.mismos(r.getDer(), r2.getDer());

            return b1 && b2;
        } else {
            return false;
        }
    }

    /**
     * 14. Dado dos lista Simples Enlazadas L1 y L2. L1 contiene el recorrido en
     * Preorden de un árbol y L2 contiene el recorrido en Inorden. Elaborar
     * métodos que devuelva en dos lista L3 y L4:  L3, los elementos del
     * Subárbol Izquierdo del árbol.  L4, los elementos del Subárbol Derecho
     * del árbol. // DUDA PROFESOR :( 
     */
    public ListaS<T>[] listas(ListaS<T> l1, ListaS<T> l2) {
        T vector1[] =(T[])l1.aVector();
        T vector2[] = (T[])l2.aVector(); 
        ListaS<T> l3 = new ListaS<>(); 
        ListaS<T> l4 = new ListaS<>();
        
        T vector[]; 
        vector = new ListaS<T>[2];
        
        
        
        
    }

    /*
        T aux = l1.get(0); 
     
        ListaS<T> l3 = new ListaS<>();
        ListaS<T> l4 = new ListaS<>();
        
        
        
        int i = 0 ; 
        while(!l2.get(i).equals(aux)){
            l3.insertarAlFinal(l2.get(i));
            i++; 
        }
        
        for (int j = i + 1 ; j < l2.getTamanio(); j++) {
            l4.insertarAlFinal(l2.get(i));
        }
        
        return vector ; */
    //private ListaS<T> listas( )
    /**
     * 16. Dado un árbol binario, elabore un método que retorne en una lista
     * circular la rama más larga (funciona )
     */
    public ListaS<T> ramaLarga() {

        NodoBin<T> r = this.getRaiz();
        return ramaLarga(r);

    }

    private ListaS<T> ramaLarga(NodoBin<T> r) {
        ListaS<T> lista = new ListaS<>();

        if (r == null) {
            return lista; // retorna la lista vacia   
        }
        ListaS<T> izq = ramaLarga(r.getIzq());
        ListaS<T> der = ramaLarga(r.getDer());

        if (izq.getTamanio() >= der.getTamanio()) {
            lista = izq;
        } else {
            lista = der;
        }

        lista.insertarAlInicio(r.getInfo());
        return lista;

    }

    /**
     * 17. Elabore un método que devuelva en una lista el recorrido de un árbol
     * binario por Niveles. Es decir primero los nodos del nivel 1, nivel 2,
     * ..., nive3 hasta el último nivel. ( no funciona recursivo , toca
     * iterativo )
     */
    public ListaS<T> porNiveles() throws Exception {

        NodoBin<T> r = this.getRaiz();
        if (r != null) {

            return porNiveles(r);
        } else {
            throw new Exception("lista vacia");
        }

    }

    private ListaS<T> porNiveles(NodoBin<T> r) {
        // crear estructuras de datos auxiliares 
        ListaS<T> lista = new ListaS<>();
        Cola<NodoBin<T>> cola = new Cola<>();

        if (r == null) {
            return lista; // retorne la lista vacia 
        }
        cola.enColar(r);

        while (!cola.esVacia()) {

            r = cola.deColar();
            lista.insertarAlFinal(r.getInfo());

            if (r.getIzq() != null) {
                cola.enColar(r.getIzq());

            }

            if (r.getDer() != null) {
                cola.enColar(r.getDer());
            }
        }
        return lista;
    }

    /**
     * 19. Dado un árbol binario de búsqueda y un dato “n”, elaborar un método
     * que borre el dato “n” del árbol. La integridad del árbol se debe
     * mantener. El árbol no tiene elementos repetidos. Realizar de forma
     * iterativa. ( plantearlo )
     */
    public void borrarDato(T info) {
        NodoBin<T> r = this.getRaiz();
        borrarDato(r, info);

    }

    private void borrarDato(NodoBin<T> r, T info) {

    }

    //---------------------------------------------------------------------------------------------------------------------------
    // pruebas libro 
    /**
     * dado un info buscarlo en el arbol , si lo encuentra lo retorna , sino
     * retorna -1
     */
    public boolean buscarAlejo(T info) {
        NodoBin<T> r = this.getRaiz();
        return buscarAlejo(r, info);

    }

    private boolean buscarAlejo(NodoBin<T> r, T info) {
        if (r == null) {
            return false;
        }

        int c = ((Comparable) info).compareTo(r.getInfo());

        if (c == 0) {
            return true;
        }
        if (c < 0) {
            return buscarAlejo(r.getIzq(), info);
        } else {
            return buscarAlejo(r.getDer(), info);
        }

    }

    /**
     * metodo preorden
     */
    public void preordenALejo() {
        NodoBin<T> r = this.getRaiz();
        preordenAlejo(r);

    }

    private void preordenAlejo(NodoBin<T> r) {
        if (r == null) {
            return;
        }

        System.out.print(r.getInfo() + " ");
        preordenAlejo(r.getIzq());
        preordenAlejo(r.getDer());

    }

    // metodo inorder
    public void inorderAlejo() {
        NodoBin<T> r = this.getRaiz();
        inorderAlejo(r);

    }

    private void inorderAlejo(NodoBin<T> r) {
        if (r == null) {
            return; // salgase 
        }
        inorderAlejo(r.getIzq());
        System.out.print(r.getInfo() + " ");
        inorderAlejo(r.getDer());

    }

    // metodo postorder
    public void postorderAlejo() {
        NodoBin<T> r = this.getRaiz();
        postorderAlejo(r);
    }

    private void postorderAlejo(NodoBin<T> r) {
        if (r == null) {
            return;
        }

        postorderAlejo(r.getIzq());
        postorderAlejo(r.getDer());
        System.out.print(r.getInfo() + " ");

    }

    /**
     *
     */
    /**
     * Metodo que permite conocer el objeto raiz del Arbol AVL. <br>
     * <b>post: </b> Se retorno el objeto raiz del Arbol. <br>
     *
     * @return Un objeto de tipo T que representa el dato en la raiz del Arbol.
     */
    @Override
    public T getObjRaiz() {
        return super.getObjRaiz();
    }

    /**
     * Metodo que permite insertar un dato en el Arbol Binario de Busqueda. <br>
     * <b>post: </b> Se inserto un nuevo dato al Arbol Binario de Busqueda. <br>
     *
     * @param dato un elemento tipo T que se desea almacenar en el arbol. <br>
     * @return true si el elemento fue insertado o false en caso contrario
     */
    public boolean insertar(T dato) {
        NodoBin<T> rr = this.esta(dato) ? null : insertar(this.getRaiz(), dato);
        if (rr != null) {
            this.setRaiz(rr);
        }
        return (rr != null);
    }

    /**
     * Metodo que permite insertar un dato en el Arbol Binario de Busqueda segun
     * factor de ordenamiento. <br>
     * <b>post: </b> Se inserto ordenado un nuevo dato al Arbol Binario de
     * Busqueda. <br>
     *
     * @param r de tipo NoboBin<T> que representa la raiz del arbol. <br>
     * @param dato elemento a insertar en el arbol de forma ordenada. <br>
     * @return true si el elemento fue insertado o false en caso contrario
     */
    private NodoBin<T> insertar(NodoBin<T> r, T dato) {
        if (r == null) {
            return (new NodoBin<T>(dato, null, null));
        }
        int compara = ((Comparable) r.getInfo()).compareTo(dato);
        if (compara > 0) {
            r.setIzq(insertar(r.getIzq(), dato));
        } else if (compara < 0) {
            r.setDer(insertar(r.getDer(), dato));
        } else {
            System.err.println("Error dato duplicado:" + dato.toString());
        }
        return r;
    }

    /**
     * Metodo que permite borrar un elmento del Arbol Binario de Busqueda. <br>
     * <b>post: </b> Se elimino el elemento en el Arbol Binario de Busqueda.
     * <br>
     *
     * @param x dato que se desea eliminar. <br>
     * @return el dato borrado o null si no lo encontro
     */
    @Override
    public boolean eliminar(T x) {
        if (!this.esta(x)) {
            return (false);
        }
        NodoBin<T> z = eliminarABB(this.getRaiz(), x);
        this.setRaiz(z);
        return (true);
    }

    /**
     * Metodo de tipo privado que permite eliminar un dato en el Arbol Binario
     * de Busqueda segun factor de ordenamiento, manteniendo su propiedad de
     * orden, para esto se busca el menor de los derechos y lo intercambia por
     * el dato que desea eliminar. La idea del algoritmo es que el dato a
     * eliminar se coloque en una hoja o en un nodo que no tenga una de sus
     * ramas. <br>
     * <b>post: </b> Se elimino el elemento Arbol Binario de Busqueda. <br>
     *
     * @param r de tipo NoboBin<T> que representa la raiz del arbol. <br>
     * @param dato elemento que se desea eliminar del arbol. <br>
     * @return el dato borrado o null si no lo encontro
     */
    private NodoBin<T> eliminarABB(NodoBin<T> r, T x) {
        if (r == null) {
            return null;//<--Dato no encontrado		
        }
        int compara = ((Comparable) r.getInfo()).compareTo(x);
        if (compara > 0) {
            r.setIzq(eliminarABB(r.getIzq(), x));
        } else if (compara < 0) {
            r.setDer(eliminarABB(r.getDer(), x));
        } else {
            if (r.getIzq() != null && r.getDer() != null) {
                NodoBin<T> cambiar = this.masIzquierda(r.getDer());
                T aux = cambiar.getInfo();
                cambiar.setInfo(r.getInfo());
                r.setInfo(aux);
                r.setDer(eliminarABB(r.getDer(), x));
            } else {
                r = (r.getIzq() != null) ? r.getIzq() : r.getDer();
            }
        }
        return r;
    }

    /**
     * Metodo que busca el menor dato del arbol. El menor dato del arbol se
     * encuentra en el nodo mas izquierdo. <br>
     * <b>post: </b> Se retorno el nodo mas izquierdo del arbol. <br>
     *
     * @param r reprenta la raiz del arbol. <br>
     * @return el nodo mas izquierdo del arbol
     */
    @SuppressWarnings("empty-statement")
    protected NodoBin<T> masIzquierda(NodoBin<T> r) {
        for (; r.getIzq() != null; r = r.getIzq());
        return (r);
    }

    /**
     * Metodo que permite evaluar la existencia de un dato dentro del Arbol
     * Binario de Busqueda es necesario para que el metodo funcione que los
     * objetos almacenados en el arbol tengan sobreescrito el metodo equals.
     * <br>
     * <b>post: </b> Se retorno true si el elemento se encuentra en el Arbol.
     * <br>
     *
     * @param x representa la informacion del elemento que se encontrar en el
     * arbol. <br>
     * @return un boolean , true si el dato esta o false en caso contrario.
     */
    public boolean estaABB(T x) {
        return (esta(this.getRaiz(), x));
    }

    /**
     * Metodo que permite conocer si un elemento especifico se encuentra en el
     * arbol. <br>
     * <b>post: </b> Se retorno true si el elemento se encuentra en el arbol.
     * <br>
     *
     * @param r representa la raiz del arbol. <br>
     * @param x representa la informacion del elemento que se encontrar en el
     * arbol. <br>
     * @return un boolean , true si el dato esta o false en caso contrario.
     */
    private boolean esta(NodoBin<T> r, T x) {
        if (r == null) {
            return (false);
        }
        int compara = ((Comparable) r.getInfo()).compareTo(x);
        if (compara > 0) {
            return (esta(r.getIzq(), x));
        } else if (compara < 0) {
            return (esta(r.getDer(), x));
        } else {
            return (true);
        }
    }

    /**
     * Metodo que permite consultar un elemento existente dentro del Arbol
     * Binario de Busqueda. <br>
     * <b>post: </b> Se retorno un NodoBin<T> perteneciente al dato buscado.
     * <br>
     *
     * @param info Elemento a ubicar dentro del Arbol Binario de Busqueda. <br>
     * @return Un objeto de tipo NodoBin<T> que representa el objeto buscado.
     */
    protected NodoBin<T> buscar(T info) {
        return (buscar(this.getRaiz(), info));
    }

    /**
     * Metodo que permite consultar un elemento existente dentro del Arbol
     * Binario de Busqueda. <br>
     * <b>post: </b> Se retorno un NodoBin<T> perteneciente al dato buscado.
     * <br>
     *
     * @param info Elemento a ubicar dentro del Arbol Binario de Busqueda. <br>
     * @param r Representa la raiz del Arbol. <br>
     * @return Un objeto de tipo NodoBin<T> que representa el objeto buscado.
     */
    protected NodoBin<T> buscar(NodoBin<T> r, T info) {
        if (r == null) {
            return (null);
        }
        if (r.getInfo().equals(info)) {
            return r;
        } else {
            NodoBin<T> aux = (r.getIzq() == null) ? null : buscar(r.getIzq(), info);
            if (aux != null) {
                return (aux);
            } else {
                return (r.getDer() == null) ? null : buscar(r.getDer(), info);
            }
        }
    }

    /**
     * Metodo que retorna un iterador con las hojas del Arbol Binario de
     * Busqueda. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Arbol Binario de
     * Busqueda.<br>
     *
     * @return un iterador con las hojas del Arbol Binario de Busqueda.
     */
    @Override
    public Iterator<T> getHojas() {
        return (super.getHojas());
    }

    /**
     * Metodo que permite determinar el numero de Nodo hojas dentro del Arbol
     * Binario de Busqueda. <br>
     * <b>post: </b> Se retorno el numero de hojas del Arbol de Busqueda. <br>
     *
     * @return El numero de hojas existentes en el Arbol Binario de Busqueda.
     */
    @Override
    public int contarHojas() {
        return (super.contarHojas());
    }

    /**
     * Metodo que retorna un iterador con el recorrido preOrden del Arbol
     * Binario de Busqueda. <br>
     * <b>post: </b> Se retorno un iterador en preOrden para el arbol. <br>
     *
     * @return un iterador en preorden (primero la raiz luego los hijos) para el
     * Arbol Binario de Busqueda.
     */
    @Override
    public Iterator<T> preOrden() {
        return (super.preOrden());
    }

    /**
     * Metodo que retorna un iterador con el recorrido in Orden del Arbol
     * Binario. <br>
     * <b>post: </b> Se retorno un iterador inOrden para el arbol. <br>
     *
     * @return un iterador en inOrden (primero el hijo izquierdo luego la raiz y
     * despues el hijo derecho) para el Arbol Binario de Busqueda. <br>
     */
    @Override
    public Iterator<T> inOrden() {
        return (super.inOrden());
    }

    /**
     * Metodo que retorna un iterador con el recorrido pos Orden del Arbol
     * Binario de Busqueda. <br>
     * <b>post: </b> Se retorno un iterador postOrden para el arbol.<br>
     *
     * @return un iterador en postOrden (primero los hijos y luego la raiz) para
     * el Arbol Binario de Busqueda. <br>
     */
    @Override
    public Iterator<T> postOrden() {
        return (super.postOrden());
    }

    /**
     * Metodo que permite retornar un iterador con el recorrido por niveles del
     * Arbol Binario de Busqueda. <br>
     * <b>post: </b> Se retorno el recorrido por niveles del Arbol Binario de
     * Busqueda. <br>
     *
     * @return un un iterador con el recorrido por niveles del Arbol Binario de
     * Busqueda.
     */
    @Override
    public Iterator<T> impNiveles() {
        return (super.impNiveles());
    }

    /**
     * Metodo que permite obtener el peso del Arbol Binario de Busqueda. <br>
     * <b>post: </b> Se retorno el numero de elementos en el Arbol Binario de
     * Busqueda. <br>
     *
     * @return Un entero con la cantidad de elementos del Arbol Binario de
     * Busqueda.
     */
    @Override
    public int getPeso() {
        return (super.getPeso());
    }

    /**
     * Metodo que permite saber si el arbol se encuentra vacio. <br>
     * <b>post: </b> Se retorno true si el arbol no contiene elementos. <br>
     *
     * @return true si no hay datos en el arbol
     */
    @Override
    public boolean esVacio() {
        return (super.esVacio());
    }

    /**
     * Metodo que permite obtener la altura del Arbol Binario de Busqueda. <br>
     * <b>post: </b> Se retorno la altura del Arbol Binario de Busqueda.<br>
     *
     * @return Un entero con la altura del Arbol Binario de Busqueda.
     */
    @Override
    public int getAltura() {
        return (super.getAltura());
    }

    /**
     * Metodo que permite clonar la informacion de un ArbolBinario de Busqueda y
     * retornarla en un nuevo Arbol. <br>
     *
     * @return Un nuevo ArbolBinarioBusqueda con la informacion clonada del
     * actual Arbol.
     */
    @Override
    public ArbolBinarioBusqueda<T> clonar() {
        ArbolBinarioBusqueda<T> t = new ArbolBinarioBusqueda<T>();
        t.setRaiz(clonarABB(this.getRaiz()));
        return (t);
    }

    private NodoBin<T> clonarABB(NodoBin<T> r) {
        if (r == null) {
            return r;
        } else {
            NodoBin<T> aux = new NodoBin<T>(r.getInfo(), clonarABB(r.getIzq()), clonarABB(r.getDer()));
            return aux;
        }
    }

    /**
     * Metodo que permite conocer por consola la informacion del Arbol Binario.
     */
    @Override
    public void imprime() {
        System.out.println(" ----- Arbol Binario de Busqueda ----- ");
        this.imprimeABB(super.getRaiz());
    }

    /**
     * Metodo de tipo privado que permite mostrar por consola la informacion del
     * Arbol Binario. <br>
     *
     * @param n Representa la raiz del ArbolBinario o de alguno de sus
     * subarboles.
     */
    public void imprimeABB(NodoBin<T> n) {
        T l = null;
        T r = null;
        if (n == null) {
            return;
        }
        if (n.getIzq() != null) {
            l = n.getIzq().getInfo();
        }
        if (n.getDer() != null) {
            r = n.getDer().getInfo();
        }
        System.out.println("NodoIzq: " + l + "\t Info: " + n.getInfo() + "\t NodoDer: " + r);
        if (n.getIzq() != null) {
            imprimeABB(n.getIzq());
        }
        if (n.getDer() != null) {
            imprimeABB(n.getDer());
        }
    }

}// Fin de la Clase ArbolBinarioBusqueda.
